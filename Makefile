# ************************************************************************** #
#                                                                            #
#                                                        :::      ::::::::   #
#   Makefile                                           :+:      :+:    :+:   #
#                                                    +:+ +:+         +:+     #
#   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        #
#                                                +#+#+#+#+#+   +#+           #
#   Created: 2016/02/29 23:55:49 by tjarross          #+#    #+#             #
#   Updated: 2016/02/29 23:55:49 by tjarross         ###   ########.fr       #
#                                                                            #
# ************************************************************************** #

LIB =		libutils.a
SRC_NAME =	memswap.c waitm.c strins.c strdel.c strsplit.c strccpy.c strsdel.c
SRC =		$(addprefix src/, $(SRC_NAME))
OBJ =		$(SRC:.c=.o)
INC =		./include/libutils.h
CC =		gcc
FLAGS = 	-Wall -Werror -Wextra -I ./include

all: $(LIB)

msg:
	@echo "\033[0;29mMaking Libft : \c"

$(LIB): msg $(OBJ)
	@ar rcs $(LIB) $(OBJ)
	@ranlib $(LIB)
	@echo "\n\033[0;34mLibft Created !\033[0;29m"

%.o: %.c $(INC)
	@$(CC) $(FLAGS) -o $@ -c $<
	@echo "\033[0;32m.\033[0;29m\c"

clean:
	@echo "\033[0;31mCleaning Libft Objects..."
	@rm -f $(OBJ)

fclean: clean
	@echo "\033[0;31mCleaning Libft Library...\033[0;29m"
	@rm -f $(LIB)

re: fclean all

.PHONY: all clean fclean re msg
