#ifndef LIBUTILS_H
# define LIBUTILS_H

# include <stdlib.h>
# include <time.h>
# include <string.h>
# include <ctype.h>

typedef unsigned int		uint;
typedef unsigned long		ulong;
typedef unsigned char		uchar;
typedef unsigned short		ushort;
typedef unsigned long long	ullong;

void	memswap(void *a, void *b, size_t size);
void	waitm(uint milliseconds);
char	*strins(const char *str, const char *substr, size_t index);
char	*strdel(const char *str, size_t index, size_t len);
char	**strsplit(const char *str, char delim);
char	*strccpy(char *dest, const char *src, char c);
char	*strsdel(const char *str, const char *substr);
int		strccmp(char *s1, const char *s2, char c);

#endif
