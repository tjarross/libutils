#include "libutils.h"

/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * This function is desined to insert a substring in a string
 * The first parameter is the main string
 * The second parameter is the substring to insert in the main string
 * The third parameter is the index to insert
 *
 *
 * Function Explanation:
 *
 * The 'strins' function first allocate a destination string
 * It copy 'str' in destination up to 'index' bytes
 * It insert the substring and finaly copy the last bytes of 'str'
 *
 *
 * Return Value:
 *
 * If success, a pointer of the begin of the destination string is returned
 * Instead, if 'str' is NULL or 'substr' is NULL or 'index' is greater than
 * the len of 'str' a NULL pointer is returned.
 */
char *strins(const char *str, const char *substr, size_t index)
{
	char	*dest;

	if (!str || !substr || (index > strlen(str)))
		return (NULL);
	if (!(dest = (char *)malloc(sizeof(char)
		* (strlen(str) + strlen(substr) + 1))))
			return (NULL);
	strncpy(dest, str, index);
	strcpy(&dest[index], substr);
	strcpy(&dest[index + strlen(substr)], &str[index]);
	dest[strlen(str) + strlen(substr)] = '\0';
	return (dest);
}
