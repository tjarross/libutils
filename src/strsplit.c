#include "libutils.h"

/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * This function is designed to split a string into substring by a delimiter
 * The first parameter is the string to split
 * The second parameter is the character delimiter
 *
 *
 * Function Explanation:
 *
 * The 'strsplit' function first count the words to allocate sufficient
 * memory to the 2D string array.
 * Then it iterates on the number of words to first count the length of the
 * word, allocate it, fill it, and indexing to the next word in 'str'
 * if there is two delimiter near by, an empty string is allocated and
 * the process continues.
 *
 *
 * Return Value:
 *
 * If success, a pointer on the begining of a new allocated string is returned
 * Instead, if 'str' is NULL, if 'delim' is not ascii character, if there is no
 * words or if the allocation fails, a NULL pointer is returned
 */
static int	count_words(const char *str, char delim)
{
	int		i;
	int		word;

	i = -1;
	word = 1;
	while (str[++i])
		if (str[i] == delim)
			++word;
	return (word);
}

static int	get_wordlen(const char *str, char delim)
{
	int	i;

	i = 0;
	while (str[i] != delim && str[i])
		++i;
	return (i);
}

char		**strsplit(const char *str, char delim)
{
	char	**dest;
	int		words;
	int		i;
	int		j;
	int		word_len;

	i = 0;
	j = -1;
	if (!str || !isascii(delim) || !(words = count_words(str, delim)))
		return (NULL);
	if (!(dest = (char **)malloc(sizeof(char *) * (words + 1))))
		return (NULL);
	dest[words] = NULL;
	while (++j < words)
	{
		word_len = get_wordlen(str + i, delim);
		if (!(dest[j] = (char *)malloc(sizeof(char) * (word_len + 1))))
			return (NULL);
		dest[j] = strncpy(dest[j], str + i, word_len);
		i += word_len + 1;
	}
	return (dest);
}
