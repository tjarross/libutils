#include "libutils.h"

/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * The 'strccpy' function is designed to copy a string to another
 * up to the 'c' character
 * The first parameter is the destination string
 * The second parameter is the source string
 * The third parameter is the stopping character
 *
 *
 * Function Explanation:
 *
 * The 'strccpy' function simply copy the source string to the destination one
 * up to 'c' character. If 'c' is not in the string the function is working
 * like 'strcpy'
 *
 *
 * Return Value:
 *
 * If success, a pointer to the destination string is returned.
 * Instead, errors occurs if 'dest' is NULL or if 'src' is NULL
 * and a NULL pointer is returned
 */
char	*strccpy(char *dest, const char *src, char c)
{
	int i;

	if (!dest || !src)
		return (NULL);
	i = 0;
	while (src[i] != c && src[i])
	{
		dest[i] = src[i];
		++i;
	}
	return (dest);
}
