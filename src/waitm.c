#include "libutils.h"

/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * This function is deigned to wait for a gived number of milliseconds
 * Its first parameter is an unsigned integer
 * that contains the milliseconds to wait
 *
 *
 * Function Explanation:
 *
 * First get the current clock and then loop while the difference of the
 * first clock value minus the second one is strictly less than the waiting time
 *
 *
 * Return Value:
 *
 * Nothing is returned
 */
void	waitm(uint milliseconds)
{
	clock_t	ticks1;
	clock_t	ticks2;
	float	seconds;

	ticks1 = clock();
	ticks2 = ticks1;
	seconds = milliseconds / 1000.f;
	while((ticks2 / (float)CLOCKS_PER_SEC
		- ticks1 / (float)CLOCKS_PER_SEC) < seconds)
		ticks2 = clock();
}
