#include "libutils.h"

/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * The 'strsdel' function is designed to delete a substring for a string
 * The first parameter is the string to operate
 * The second parameter is the substring to remove in the main string
 *
 *
 * Function Explanation:
 *
 * This function gets the index of 'substr' in 'str' and 'substr' length
 * and send these informations to the 'strdel' function
 *
 *
 * Return Value:
 *
 * Check out the 'Return Value' section from the 'strdel' function
 * located in 'strdel.c'
 */
char *strsdel(const char *str, const char *substr)
{
	return (strdel(str, (ullong)(strstr(str, substr) - str), strlen(substr)));
}
