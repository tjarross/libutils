#include "libutils.h"

/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * The 'strccmp' function is designed to compare a string with another
 * up to the 'c' character
 * The first parameter is the string to compare
 * The second parameter is the other string to compare
 * The third parameter is the stopping character
 *
 *
 * Function Explanation:
 *
 * The 'strccmp' function simply compare the 's1' string with the 's2' string
 * up to 'c' character in 's1'. If 'c' is not in the string the function is working
 * like 'strcmp'
 *
 *
 * Return Value:
 *
 * The 'strccmp' function return an integer less than, equal to,
 * or greater than zero if s1 is found, respectively, to be less than,
 * to match, or be greater than s2.
 */
int	strccmp(char *s1, const char *s2, char c)
{
	int i;

	i = 0;
	while (s1[i] != c && s1[i])
	{
		if (s1[i] != s2[i])
			return (s1[i] - s2[i]);
		++i;
	}
	return (0);
}
