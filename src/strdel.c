#include "libutils.h"
#include <stdio.h>

/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * This function is desined to delete a part of a string
 * The first parameter is the string to operate
 * The second parameter is the string index
 * The third parameter is the length to delete
 *
 *
 * Function Explanation:
 *
 * The 'strdel' function first allocate a destination string
 * It copy 'str' in destination up to 'index' bytes
 * It skip 'len' bytes of 'str' and copy the rest to 'dest'
 *
 *
 * Return Value:
 *
 * If success, a pointer of the begin of the destination string is returned
 * Instead, an error occurs if malloc fail, if 'str' is NULL, if 'index' is
 * greater tha 'str' length or if the len provided overflow the string
 * and a NULL pointer is returned
 */
char *strdel(const char *str, size_t index, size_t len)
{
	char	*dest;

	if (!str || index > strlen(str) || (index + len) > strlen(str))
		return (NULL);
	if (!(dest = (char *)malloc(sizeof(char) * (strlen(str) - len + 1))))
		return (NULL);
	strncpy(dest, str, index);
	strcpy(&dest[index], &str[index + len]);
	dest[strlen(str) - len] = '\0';
	return (dest);
}
