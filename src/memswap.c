#include "libutils.h"
/*
 * Developped by: Thomas JARROSSAY
 * Mail: jarrossay.thomas@gmail.com
 *
 *
 * Description:
 *
 * This function is designed to swap
 * up to 'size' bytes the memory of two buffers
 * The first and the second parameter is the first pointer to swap
 * The third parameter is the size in bytes
 *
 *
 * Function Explanation:
 *
 * The 'memswap' function simply swap up to 'size' bytes
 * of pointers 'a' and 'b' using xor swap method
 *
 *
 * Return Value:
 *
 * Nothing is returned
 */
void ft_memswap(void *a, void *b, size_t size)
{
	while (size--)
	{
    	if (((char *)a)[size] != ((char *)b)[size])
        {
			((char *)a)[size] ^= ((char *)b)[size];
			((char *)b)[size] ^= ((char *)a)[size];
			((char *)a)[size] ^= ((char *)b)[size];
		}
	}
}
